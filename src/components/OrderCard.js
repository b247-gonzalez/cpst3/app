import { useEffect, useState } from "react";
import { Col, Row, Stack } from "react-bootstrap";

export default function OrderCard({order}) {
    const {productId, quantity, total, isPaid, status} = order;
    const [productName, setProductName] = useState("");
    const [productDescription, setProductDescription] = useState("");

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/view/${productId}`)
        .then(res => res.json())
        .then(data => {
            setProductName(data.title);
            setProductDescription(data.description);
        })
    })

    const toPrice = (number) => {
        let num = number.toFixed(2);
        num = num.split('.');
        let dec = num[1];
        num = parseInt(num);
        num = num.toLocaleString(undefined, {maximumFractionDigits: 0});
        return `${num}.${dec}`;
    };

    return (
        <Row className="order-item py-2">
            <Stack direction="vertical" gap={2}>
                <Stack direction="vertical" gap={0}>
                    <h3 className="title m-0">{productName}</h3>
                    <p className="m-0 description">{productDescription}</p>
                </Stack>

                <Row className="details">
                    <Col xs={3}>
                        <p><b>Quantity:</b> {quantity}</p>
                    </Col>
                    <Col xs={3}>
                        <p><b>Status:</b> {status}</p>
                    </Col>
                    <Col xs={3}>
                        <p><b>Paid:</b> {isPaid.toString()}</p>
                    </Col>
                    <Col xs={3}>
                        <p><b>Total:</b> &#8369;&nbsp;{toPrice(total)}</p>
                    </Col>
                </Row>
            </Stack>
        </Row>
    );
}