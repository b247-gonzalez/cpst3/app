import "../assets/css/LoadingComponent.css";
import { ReactComponent as Logo } from "../logo.svg";
import { Col, Container, Row, Spinner, Stack } from "react-bootstrap";

export default function Loading() {
    return(
        <Container fluid={true} id="loading-container">
            <Row className="justify-content-center alignt-items-center h-100">
                <Col className="my-auto text-center detail-container">
                <Stack direction="vertical" gap={4} className="justify-content-center align-items-center">
                    <Spinner animation="border" />
                    <Logo title="Logo" className="logo" />
                </Stack>
                </Col>
            </Row>
        </Container>
    );
}