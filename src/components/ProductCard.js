import { useEffect, useState } from "react";
import "../assets/css/ProductCard.css";
import { Card, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProductCard({product}) {
    const {_id, title, price, discount, thumbnail} = product;
    const [discountedPrice, setDiscountedPrice] = useState(0);

    useEffect(() => {
        if (discount !== 0) {
            setDiscountedPrice(price*((100 - discount)/100));
        }
    }, [discount, price]);

    const toPrice = (number) => {
        let num = number.toFixed(2);
        num = num.split('.');
        let dec = num[1];
        num = parseInt(num);
        num = num.toLocaleString(undefined, {maximumFractionDigits: 0});
        return `${num}.${dec}`;
    };

    return (
    <Col xs={5} md={2}>
        <Card className="productCard h-100" as={Link} to={`/product/${_id}`} >
            <Card.Img variant="top" src={thumbnail} />

            <Card.Body>
                <Card.Title className="title">{title}</Card.Title>
                <Card.Text className="price  my-0 py-0">
                    <span className="currency">&#8369;</span>&ensp;{ discount !== 0 ? toPrice(discountedPrice) : toPrice(price) }
                </Card.Text>

                {discount !== 0
                ?
                <Card.Text className="discount">
                    <span className="text-decoration-line-through">
                        <span className="currency">&#8369;</span>&nbsp;{price}
                    </span>
                    &emsp;&ndash;&nbsp;{discount}&#37;
                </Card.Text>
                :
                <></>
                }
            </Card.Body>
        </Card>
    </Col>
    );
}