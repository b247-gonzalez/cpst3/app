import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { Button, Col, Form, Image, InputGroup, Row, Stack } from "react-bootstrap";

export default function CartCard({item}) {
    const {_id, productId, quantity, subtotal} = item;
    const [productName, setProductName] = useState("");
    const [productDescription, setProductDescription] = useState("");
    const [thumbnail, setThumbnail] = useState("");
    const [qty, setQty] = useState(0);
    const [stock, setStock] = useState(0);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/view/${productId}`, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            setProductName(data.title);
            setProductDescription(data.description);
            setThumbnail(data.thumbnail);
            setStock(data.stock);
            setQty(quantity);
        })
    });

    const deleteItem = (e) => {

    }

    const toPrice = (number) => {
        let num = number.toFixed(2);
        num = num.split('.');
        let dec = num[1];
        num = parseInt(num);
        num = num.toLocaleString(undefined, {maximumFractionDigits: 0});
        return `${num}.${dec}`;
    };

    const subQty = (e) => {
        e.preventDefault();
        if (quantity !== 0) {
            setQty(quantity - 1);
        }
    }
    
    const addQty = (e) => {
        e.preventDefault();
        if (quantity !== stock) {
            setQty(quantity + 1);
        }
    }

    return (
        <Row className="cart-item p-0 align-items-stretch">
            <Col xs={4} md={3}>
                <Image src={thumbnail} alt="thumbnail" className="thumbnail" />
            </Col>

            <Col xs={8} md={9} className="details">
                <Row className="h-100">
                    <Col xs={12} lg={8}>
                        <h5 className="title m-0 prod-name">{productName}</h5>
                        <p className="description">{productDescription}</p>
                        <p className="price">&#8369;&nbsp;{toPrice(subtotal)}</p>
                    </Col>
                    
                    <Col xs={12} lg={4} className="mt-3 m-lg-0 manipulation">
                        <InputGroup className="qty-setter mb-1 mb-lg-2 justify-content-center">
                            <Button variant="accent-2" className="btnMinus" onClick={subQty}><FontAwesomeIcon icon="fa-solid fa-minus" /></Button>
                            <Form.Control aria-label="quantity" value={qty} onChange={e => setQty(e.target.value)} className="text-center item-qty" />
                            <Button variant="accent-2" className="btnAdd" onClick={addQty}><FontAwesomeIcon icon="fa-solid fa-plus" /></Button>
                        </InputGroup>
                        <Stack direction="horizontal" gap={2} className="justify-content-end justify-content-lg-center">
                            <Button id="btnDelete" variant="danger" onClick={deleteItem}><FontAwesomeIcon icon="fa-solid fa-trash-can" /></Button>
                            <Button id="btnCheckout" variant="primary">Checkout</Button>
                        </Stack>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
}