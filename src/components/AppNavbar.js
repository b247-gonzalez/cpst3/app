import UserContext from "../UserContext";
import { ReactComponent as Logo } from "../logo.svg";
import { Badge, Button, Container, Form, Nav, NavDropdown, Navbar } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Link, NavLink } from "react-router-dom";
import { Fragment, useContext, useEffect, useState } from "react";

export default function AppNavbar() {
    const { user } = useContext(UserContext);
    const [search, setSearch] = useState("");
    const [isSearchEmpty, setIsSearchEmpty] = useState(true);
    const [cartCount, setCartCount] = useState(0);

    useEffect(() => {
        if (search !== '') {
            setIsSearchEmpty(false);
        }
        else {
            setIsSearchEmpty(true);
        }
    },[search, isSearchEmpty]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user/cart`, {
            method: 'GET',
            headers : {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setCartCount(data.length);
        });
    }, []);

    return (
    <Fragment>
        <Navbar bg="accent-1" expand="lg" className="py-3">
            <Container>
                <Navbar.Brand as={Link} to="/" className="m-0 p-0"><Logo title="Logo" className="logo" /></Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarNav" />
                <Navbar.Collapse id="navbarNav" className="pt-3 p-lg-0 d-md-flex flex-md-row align-items-center">
                    <Form className="mx-0 mx-lg-4 mb-3 mb-md-0 me-md-3 d-flex flex-fill" role="search">
                        <Form.Group className="input-group border-accent" id="searchbox" controlId="prodSearch">
                        <Form.Control type="search" name="prodSearch" className="searchbox" placeholder="Search for products" aria-label="Search for products" aria-describedby="button-search" onKeyUp={e => setSearch(e.target.value)} />
                        <Button type="reset" variant="text" className={["clear-search", isSearchEmpty ? "hidden" : "show"]}><FontAwesomeIcon icon="fa-solid fa-xmark" /></Button>
                        <Button type="submit" id="button-search" variant="search" className="px-4 bg-accent-2"><FontAwesomeIcon icon="fa-solid fa-magnifying-glass" /></Button>
                        </Form.Group>
                    </Form>
                    { user.id !== null
                        ?
                        <Nav className="flex-row align-items-center justify-content-end">
                        <Nav.Item>
                            <Nav.Link as={Link} to="/user/cart" className="cart-icon position-relative">
                                <FontAwesomeIcon icon="fa-solid fa-basket-shopping" />
                                {cartCount > 0
                                ?
                                    <Badge pill bg="primary" className="cart-count">{cartCount}</Badge>
                                :
                                <></>
                                }
                            </Nav.Link>
                        </Nav.Item>
                            <NavDropdown title={<span>{user.firstName} {user.lastName}</span>} id="user-dropdown">
                                <NavDropdown.Item as={Link} to="/user/profile"><FontAwesomeIcon icon="fa-solid fa-user" />&emsp;Manage My Account</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to="/user/orders"><FontAwesomeIcon icon="fa-solid fa-box" />&emsp;My Orders</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to="/user/settings/profile"><FontAwesomeIcon icon="fa-solid fa-user-gear" />&emsp;Account Settings</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item as={Link} to="/logout" className="text-danger"><FontAwesomeIcon icon="fa-solid fa-power-off" />&emsp;Logout</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                        :
                        <Nav className="mt-3 m-lg-0 gap-lg-2 d-flex flex-row justify-content-around">
                            <Button as={NavLink} to="/login" variant="accent-2">Login</Button>
                            <Button as={NavLink} to="/signup" variant="outline-accent">Sign Up</Button>
                        </Nav>
                    }
                </Navbar.Collapse>
            </Container>
        </Navbar>
        { user.isAdmin === true
            ?
            <Navbar bg="light" expand="lg" className="adminNavbar">
                <Container>
                <Navbar.Toggle aria-controls="navbarAdmin" />

                <Navbar.Collapse id="navbarAdmin">
                    <Nav>
                        <Nav.Link as={NavLink} to="/admin/addProduct">Add Product</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                </Container>
            </Navbar>
            :
            <></>
        }
    </Fragment>
    );
}