import { useEffect, useState } from "react";
import { Button, Col, Row, Stack } from "react-bootstrap";
import Swal from "sweetalert2";

export default function AddressCard({address}) {
    const { _id, unit, street, subdivision, district, city, postcode, province, isPrimary } = address;
    const [fullAddress, setFullAddress] =useState("");

    useEffect(() => {
        if(subdivision !== "") {
            setFullAddress(`${unit} ${street}, ${subdivision}, ${district}, ${city} City, ${postcode} ${province}`);
        }
        else {
            setFullAddress(`${unit} ${street}, ${district}, ${city} City, ${postcode} ${province}`);
        }
    },[city, district, postcode, province, street, subdivision, unit]);

    const deleteAddress = (e) => {
        e.preventDefault();
        
        fetch(`${process.env.REACT_APP_API_URL}/user/deleteAddress`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({addrId: _id})
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Address has been deleted"
                });
            }
            else {
                Swal.fire({
                    title: "Failed!",
                    icon: "error",
                    text: "Something went wrong! Please try again."
                });
            }
        });
    };

    return (
        <Row className="py-2 align-items-center addr-item">
            <Col xs={12} lg={5} className="mb-3 mb-lg-0">
                <p className="m-0">{fullAddress}</p>
            </Col>
            {isPrimary
            ?
            <Col xs={7} lg={3}>
                <Stack direction="vertical" gap={0}>
                    <p className="m-0 text-muted">DEFAULT SHIPPING ADDRESS</p>
                    <p className="m-0 text-muted">DEFAULT BILLING ADDRESS</p>
                </Stack>
            </Col>
            :
            <></>
            }
            <Col xs={isPrimary ? 5 : {span: 5, offset: 7}} lg={isPrimary ? 4 : {span: 4, offset: 3}}>
                <Stack direction="horizontal" gap={3} className="justify-content-center">
                    <Button id="btnDelete" variant="danger" className="w-50" onClick={deleteAddress}>Delete</Button>
                    <Button id="btnEdit" variant="primary" className="w-50">Edit</Button>
                </Stack>
            </Col>
        </Row>
    );
}