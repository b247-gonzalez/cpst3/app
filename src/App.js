import { Fragment, useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
import Signup from './pages/Signup';
import Logout from './pages/Logout';

import AdminLogin from './pages/AdminLogin';
import AddProduct from './pages/AddProduct';

import UserProfile from './pages/user/UserProfile';
import AccountSettings from './pages/user/AccountSettings';
import UserOrders from './pages/user/UserOrders';

import ProductView from './pages/ProductView';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
library.add(fas, far, fab);

export default function App() {
    const [user, setUser] = useState({
        id: null,
        firstName: null,
        lastName: null,
        isAdmin: null
    });

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    isAdmin: data.isAdmin
                });
            }
            else {
                setUser({
                    id: null,
                    firstName: null,
                    lastName: null,
                    isAdmin: null
                });
            }
        })
    });

    return (
        <Fragment>
            <UserProvider value={{ user, setUser, unsetUser }}>
                <Router>
                    <AppNavbar />
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/login" element={<Login />} />
                        <Route path="/signup" element={<Signup />} />
                        <Route path="/logout" element={<Logout />} />

                        <Route path="/product/:productId" element={<ProductView />} />
                        
                        <Route path="/admin/login" element={<AdminLogin />} />
                        <Route path="/admin/addProduct" element={<AddProduct />} />

                        <Route path="/user/:tab" element={<UserProfile />} />
                        <Route path="/user/settings/:tab" element={<AccountSettings />} />
                        <Route path="/user/orders" element={<UserOrders />} />
                    </Routes>
                </Router>
            </UserProvider>
        </Fragment>
    );
}