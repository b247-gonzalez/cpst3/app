import { useContext } from "react";
import UserContext from "../UserContext";
import AdminProductView from "./AdminProductView";
import UserProductView from "./UserProductView";

export default function ProductView() {
    const { user } = useContext(UserContext);
    return(
        (user.isAdmin)
        ?
        <AdminProductView />
        :
        <UserProductView />
    );
}