import { useState } from "react";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import OrderCard from "../../components/OrderCard";
import "../../assets/css/user/Orders.css"

export default function UserOrders() {
    const [orders, setOrders] = useState([]);
    const [isOrderEmpty, setIsOrderEmpty] = useState(false);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user/getOrders`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data !== false) {
                setIsOrderEmpty(false);
                setOrders(data.map(order => {
                    return (
                        <OrderCard key={order._id} order={order} />
                    );
                }));
            }
            else {
                setIsOrderEmpty(true);
                setOrders(() => {
                    return (
                        <h3 className="title">No orders yet.</h3>
                    );
                });
            }
        });
    }, []);

    return (
        <Container className="my-5">
            <h3 className="title">Orders</h3>
            
            {isOrderEmpty
            ?
            <Container className="order_list empty">
                {orders}
            </Container>
            :
            <Container className="order_list">
                {orders}
            </Container>
            }
        </Container>
    );
}