import { Col, Container, Nav, Row, Tab } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import SettingsProfile from "./SettingsProfile";
import SettingsSecurity from "./SettingsSecurity";

export default function AccountSettings() {
    const { tab } = useParams();

    return (
        <Container fluid={true}>
            <Tab.Container id="account-nav" defaultActiveKey="profile" activeKey={tab} >
                <Row className="navigation">
                    <Col xs={12} md={3} lg={2} className="navigation-tabs p-0">
                        <Nav variant="pills" className="flex-row flex-md-column">
                            <Nav.Item>
                                <Nav.Link as={Link} to="/user/settings/profile" eventKey="profile">Profile</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/user/settings/security" eventKey="security">Security</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Col>
                    <Col xs={12} md={9} lg={10} className="navigation-content">
                        <Tab.Content>
                            <Tab.Pane eventKey="profile"><SettingsProfile /></Tab.Pane>
                            <Tab.Pane eventKey="security"><SettingsSecurity /></Tab.Pane>
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </Container>
    );
}