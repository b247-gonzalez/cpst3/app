import { useEffect, useState } from "react";
import Loading from "../../components/Loading";
import { Button, Col, Container, Form, InputGroup, Row, Stack } from "react-bootstrap";
import Swal from "sweetalert2";

export default function ProfilePage() {
    const [isLoading, setIsLoading] = useState(true);
    const [editMode, setEditMode] = useState(false);
    const [email, setEmail] = useState("");
    const [hiddenEmail, setHiddenEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [hiddenMobileNumber, setHiddenMobileNumber] = useState("");

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data._id !== "undefined") {
                setEmail(data.email);
                setFirstName(data.firstName);
                setLastName(data.lastName);
                setMobileNumber(data.mobileNumber);

                setIsLoading(false);
            }
        })
    }, []);

    useEffect(() => {
        let hiddenMail = email.split('@');
        let emailDomain = hiddenMail[1];
        let emailAddr = hiddenMail[0];
        let hiddenNumber = mobileNumber.substring(0, 3) + "********";

        emailAddr = emailAddr.substring(0, 2) + "***";
        setHiddenEmail(`${emailAddr}@${emailDomain}`);
        setHiddenMobileNumber(hiddenNumber)
    }, [email, mobileNumber])

    const changeMode = (e) => {
        e.preventDefault();

        if (editMode === false) {
            setEditMode(true);
        }
        else {
            setEditMode(false);
            setIsLoading(true);

            fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
                headers: {
                    authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                setEmail(data.email);
                setFirstName(data.firstName);
                setLastName(data.lastName);
                setMobileNumber(data.mobileNumber);

                setIsLoading(false);
            });
        }
    }

    const detailSave = (e) => {
        e.preventDefault();
        setIsLoading(true);

        fetch(`${process.env.REACT_APP_API_URL}/user/updateDetails`, {
            method: 'PATCH',
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNumber: mobileNumber
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true) {
                setIsLoading(false);
                setEditMode(false);

                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Your details have been updated!"
                });
            }
            else {
                setIsLoading(false);

                Swal.fire({
                    title: "Failed!",
                    icon: "error",
                    text: "Something went wrong! Please try again."
                });
            }
        });
    };

    return (
        (isLoading)
        ?
        <Loading />
        :
        <Container className="pt-3">
            <h3 className="title">Profile</h3>

            <Container fluid={true} className="mt-2 details p-4">
                <Form>
                    <Row>
                        <Col xs={12} md={6} lg={4} className="mb-4">
                            <Form.Label>Full name</Form.Label>
                            {editMode
                            ?
                            <InputGroup className="input-group">
                                <Form.Control id="firstName" placeholder="First name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
                                <Form.Control id="lastName" placeholder="Last name" value={lastName} onChange={e => setLastName(e.target.value)} required />
                            </InputGroup>
                            :
                            <InputGroup className="input-group">
                                <Form.Text>{firstName}&nbsp;{lastName}</Form.Text>
                            </InputGroup>
                            }
                        </Col>
                        <Col xs={12} md={6} lg={4} className="mb-4">
                            <Form.Label>Email</Form.Label>
                            {editMode
                            ?
                            <InputGroup className="input-group">
                                <Form.Control id="email" value={email} onChange={e => setEmail(e.target.value)} placeholder="Email" />
                            </InputGroup>
                            :
                            <InputGroup className="input-group">
                                <Form.Text>{hiddenEmail}</Form.Text>
                            </InputGroup>
                            }
                        </Col>
                        <Col xs={12} md={6} lg={4} className="mb-4">
                            <Form.Label>Mobile number</Form.Label>
                            {editMode
                            ?
                            <InputGroup className="input-group">
                                <Form.Control id="mobileNumber" placeholder="Mobile number" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)} required />
                            </InputGroup>
                            :
                            <InputGroup className="input-group">
                                <Form.Text>{hiddenMobileNumber}</Form.Text>
                            </InputGroup>
                            }
                        </Col>
                    </Row>

                    {editMode
                    ?
                    <Stack direction="horizontal" gap={2} className="mt-md-5 justify-content-end">
                        <Button variant="danger" id="btnCancel" onClick={changeMode}>Cancel</Button>
                        <Button variant="primary" id="btnSave" onClick={detailSave}>Save</Button>
                    </Stack>
                    :
                    <Stack direction="horizontal" className="mt-md-5 justify-content-end">
                        <Button variant="primary" id="btnEdit" className="w-25" onClick={changeMode}>Edit</Button>
                    </Stack>
                    }
                </Form>
            </Container>
        </Container>
    );
}