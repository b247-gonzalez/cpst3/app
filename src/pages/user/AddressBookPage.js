import "../../assets/css/user/AddressBook.css";
import { useEffect, useState } from "react";
import { Badge, Button, Col, Container, Form, Modal, Row } from "react-bootstrap";
import AddressCard from "../../components/AddressCard";
import Loading from "../../components/Loading";
import Swal from "sweetalert2";

export default function AddressBookPage() {
    const [ addrs, setAddress] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isAddrEmpty, setIsAddrEmpty] = useState(true);
    const [isAddrModalShow, setIsAddrModalShow] = useState(false);
    const [unit, setUnit] = useState("");
    const [street, setStreet] = useState("");
    const [subdivision, setSubdivision] = useState("");
    const [district, setDistrict] = useState("");
    const [city, setCity] = useState("");
    const [postcode, setPostcode] = useState("");
    const [province, setProvince] = useState("");

    useEffect(() => {
        setIsLoading(true);
        
        fetch(`${process.env.REACT_APP_API_URL}/user/addressList`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data !== false) {
                setIsLoading(false);
                setIsAddrEmpty(false);

                setAddress(data.map(address => {
                    return (
                        <AddressCard key={address._id} address={address} />
                    );
                }));
            }
            else {
                setIsLoading(false);
                setIsAddrEmpty(true);

                setAddress(() => {
                    return (
                        <div className="btnContainer">
                            <h5 className="title">You have no address yet.</h5>
                            <p>Please add an address first</p>
                            <Button variant="accent-2" className="btnAdd" id="btnAdd" onClick={e => setIsAddrModalShow(true)}>Add Address</Button>
                        </div>
                    );
                });
            }
        });
    }, []);

    const clearAddrModal = (e) => {
        e.preventDefault();

        setUnit("")
        setStreet("");
        setSubdivision("");
        setDistrict("");
        setCity("");
        setPostcode("");
        setProvince("");
        setIsAddrModalShow(false);
    }
    
    const newAddress = (e) => {
        e.preventDefault();
        setIsLoading(true);
        
        fetch(`${process.env.REACT_APP_API_URL}/user/updateAddress`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                unit : unit,
                street : street,
                subdivision : subdivision,
                district : district,
                city : city,
                postcode : postcode,
                province : province
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                setIsLoading(false);

                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Address has been successfully added"
                });

                setUnit("")
                setStreet("");
                setSubdivision("");
                setDistrict("");
                setCity("");
                setPostcode("");
                setProvince("");
                setIsAddrModalShow(false);
            }
        })
    }

    return (
        (isLoading)
        ?
        <Loading />
        :
        <Container className="pt-3" id="addr_container">
            <h3 className="title">Address Book</h3>
            {isAddrEmpty
            ?
            <Container className="addr_list empty">
                {addrs}
            </Container>
            :
            <Row>
                <Container className="addr_list">
                {addrs}
                </Container>

                <Button variant="accent-2" className="btnAdd mt-5 ms-auto w-25" id="btnAdd" onClick={e => setIsAddrModalShow(true)}>Add Address</Button>
            </Row>
            }

            <Modal show={isAddrModalShow} onHide={e => setIsAddrModalShow(false)} size="lg" centered>
                <Modal.Header closeButton>
                    <Modal.Title className="title">Add New Address</Modal.Title>
                </Modal.Header>
                
                <Modal.Body>
                    <Row>
                        <Col xs={6} lg={3} className="mb-3">
                            <Form.Group controlId="unit">
                                <Form.Label className="position-relative">Unit/Block & Lot<Badge bg="transparent" className="text-danger field-required">*</Badge></Form.Label>
                                <Form.Control type="text" placeholder="No. 333" value={unit} onChange={e => setUnit(e.target.value)} required />
                            </Form.Group>
                        </Col>
                        <Col xs={6} lg={4} className="mb-3">
                            <Form.Group controlId="street">
                                <Form.Label className="position-relative">Street<Badge bg="transparent" className="text-danger field-required">*</Badge></Form.Label>
                                <Form.Control type="text" placeholder="Padre Burgos Ave." value={street} onChange={e => setStreet(e.target.value)} required />
                            </Form.Group>
                        </Col>
                        <Col xs={6} lg={5} className="mb-3">
                            <Form.Group controlId="subdv">
                                <Form.Label>Subdivision</Form.Label>
                                <Form.Control type="text" placeholder="Subdivision/Compound/Village" value={subdivision} onChange={e => setSubdivision(e.target.value)} />
                            </Form.Group>
                        </Col>
                        <Col xs={6} lg={3} className="mb-3 mb-lg-0">
                            <Form.Group controlId="brgy">
                                <Form.Label className="position-relative">Barangay<Badge bg="transparent" className="text-danger field-required">*</Badge></Form.Label>
                                <Form.Control type="text" placeholder="Ermita" value={district} onChange={e => setDistrict(e.target.value)} required />
                            </Form.Group>
                        </Col>
                        <Col xs={4} lg={3}>
                            <Form.Group controlId="city">
                                <Form.Label className="position-relative">City/Municipality<Badge bg="transparent" className="text-danger field-required">*</Badge></Form.Label>
                                <Form.Control type="text" placeholder="Manila" value={city} onChange={e => setCity(e.target.value)} required />
                            </Form.Group>
                        </Col>
                        <Col xs={3} lg={2}>
                            <Form.Group controlId="postcode">
                                <Form.Label className="position-relative">Postal Code<Badge bg="transparent" className="text-danger field-required">*</Badge></Form.Label>
                                <Form.Control type="number" placeholder="1000" value={postcode} onChange={e => setPostcode(e.target.value)} required />
                            </Form.Group>
                        </Col>
                        <Col xs={5} lg={4}>
                            <Form.Group controlId="postcode">
                                <Form.Label className="position-relative">Province<Badge bg="transparent" className="text-danger field-required">*</Badge></Form.Label>
                                <Form.Control type="text" placeholder="Metro Manila" value={province} onChange={e => setProvince(e.target.value)} className="text-uppercase" required />
                            </Form.Group>
                        </Col>
                    </Row>
                </Modal.Body>
                
                <Modal.Footer>
                    <Button type="button" variant="secondary" id="btnCancel" onClick={clearAddrModal}>Cancel</Button>
                    <Button type="button" variant="accent-2" id="btnSave" onClick={newAddress}>Create</Button>
                </Modal.Footer>
            </Modal>
        </Container>
    );
}