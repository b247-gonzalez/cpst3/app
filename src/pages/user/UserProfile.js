import "../../assets/css/user/Profile.css"
import { Col, Container, Nav, Row, Tab } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import ProfilePage from "./ProfilePage";
import AddressBookPage from "./AddressBookPage";
import CartView from "./CartView";

export default function UserProfile() {
    const { tab } = useParams();

    return (
        <Container fluid={true}>
            <Tab.Container id="account-nav" defaultActiveKey="profile" activeKey={tab} >
                <Row className="navigation">
                    <Col xs={12} md={3} lg={2} className="navigation-tabs p-0">
                        <Nav variant="pills" className="flex-row flex-md-column">
                            <Nav.Item>
                                <Nav.Link as={Link} to="/user/profile" eventKey="profile">Profile</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/user/addressBook" eventKey="addressBook">Address Book</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/user/cart" eventKey="cart">Cart</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Col>
                    <Col xs={12} md={9} lg={10} className="navigation-content">
                        <Tab.Content>
                            <Tab.Pane eventKey="profile"><ProfilePage /></Tab.Pane>
                            <Tab.Pane eventKey="addressBook"><AddressBookPage /></Tab.Pane>
                            <Tab.Pane eventKey="cart"><CartView /></Tab.Pane>
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </Container>
    );
}