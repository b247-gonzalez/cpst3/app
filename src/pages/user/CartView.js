import "../../assets/css/user/Cart.css"
import CartCard from "../../components/CartCard";
import { useEffect, useState } from "react";
import { Button, Container, Stack } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CartView() {
    const [cart, setCart] = useState([]);
    const [isEmpty, setIsEmpty] = useState(true);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user/cart`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                setIsEmpty(false);
                setCart(data.map(item => {
                    return (
                        <CartCard key={item._id} item={item} />
                    );
                }));
            }
            else {
                setIsEmpty(true);
            }
        });
    });

    return (
        <Container className="pt-3" id="cart-container">
            <h3 className="title">Cart</h3>
            
            {!isEmpty
            ?
                <Container className="cart-list">
                    {cart}
                </Container>
            :
                <Container className="empty-container">
                    <Stack direction="vertical" gap={2} className="w-50 text-center mx-auto">
                        <h4 className="title">Cart is empty</h4>
                        <p className="subtitle fst-normal">View products and add them to your cart!</p>

                        <Button as={Link} to="/" variant="primary" className="w-25 mx-auto">View Products</Button>
                    </Stack>
                </Container>
            }
        </Container>
    );
}