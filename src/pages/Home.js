import UserContext from "../UserContext";
import { useContext } from "react";
import { Container } from "react-bootstrap";
import ProductsAll from "./ProductsAll";
import ProductsActive from "./ProductsActive";

export default function Home() {
    const { user } = useContext(UserContext);
    
    return (
        <Container className="pt-5 d-flex flex-wrap align-items-stretch gap-3">
        { user.isAdmin
            ?
            <ProductsAll />
            :
            <ProductsActive />
        }
        </Container>
    );
}