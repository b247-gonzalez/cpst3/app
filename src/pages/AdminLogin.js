import "../assets/css/Login.css"
import { useContext, useEffect, useState } from "react";
import { Link, NavLink, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function AdminLogin() {
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isValid, setIsValid] = useState(false);
    // eslint-disable-next-line no-useless-escape
    const passReg = RegExp(/^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{6,}$/g);
    const emailReg = RegExp(/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);

    function loginUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/admin/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "You have successfully logged in!"
                });
            }
            else {
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "Login failed! Please ensure you have input correct credentials."
                });
            }
        });

        setEmail("");
        setPassword("");
    };

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });
        });
    };

    useEffect(() => {
        if (((email !== "") && (emailReg.test(email))) && ((password !== "") && (password.length >= 8)) && (passReg.test(password))) {
            setIsValid(true);
        }
        else {
            setIsValid(false);
        }
    }, [email, password, emailReg, passReg]);

    return (
        (user.id !== null)
        ?
            <Navigate to="/" />
        :
        <Container id="loginForm" className="pt-5">
            <Row>
                <Col xs={8}>
                    <h4 className="title">Welcome to E-Commerce!</h4>
                    <h5 className="subtitle fst-normal">Please login</h5>
                </Col>
                
                <Col xs={4} className="d-flex align-items-center justify-content-end">
                    <p className="">New member? <Link to="/signup">Register</Link> here.</p>
                </Col>
            </Row>
            <Form onSubmit={loginUser}>
            <Row>
                <Col xs={12} md={7}>
                    <Form.Group controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email here" value={email} onChange={e => setEmail(e.target.value)} required />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Enter password here" value={password} onChange={e => setPassword(e.target.value)} required />
                    </Form.Group>
                    <Button as={NavLink} to="/forgotpassword" variant="link" size="sm">forgot password?</Button>
                </Col>

                <Col xs={12} md={5}>
                { isValid
                    ?
                        <Button type="submit" variant="accent-2" id="btnLogin" className="w-100">Login</Button>
                    :
                        <Button type="submit" variant="accent-2" id="btnLogin" className="w-100" disabled>Login</Button>
                }
                <p className="text-center my-2">or</p>
                <div className="d-flex flex-column gap-3 justify-content-around">
                    <Button type="button" variant="facebook" id="btnFacebook" className="w-100 fw-bold" disabled><FontAwesomeIcon icon="fa-brands fa-facebook-f" />&emsp;Facebook</Button>
                    <Button type="button" variant="google" id="btnGoogle" className="w-100 fw-bold" disabled><FontAwesomeIcon icon="fa-brands fa-google-plus-g" />&emsp;Google</Button>
                </div>
                </Col>
            </Row>

            </Form> 
        </Container>
    );
};