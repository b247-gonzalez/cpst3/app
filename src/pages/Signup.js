import "../assets/css/Signup.css"
import UserContext from "../UserContext";
import Loading from "../components/Loading";
import { useContext, useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Signup() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    const [isLoading, setIsLoading] = useState(false);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);
    const nameReg = RegExp(/^[^-\s][a-zA-Z_\s-]+$/);
    const passReg = RegExp(/^[a-zA-Z0-9!@#$%^&*]{6,}$/);
    const mobileReg = RegExp(/^[0-9]{6,}$/);
    const emailReg = RegExp(/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);

    function register(e) {
        let fname = firstName.split(" ");
        let lname = lastName.split(" ");

        fname = fname.map((name) => {
            return name[0].toUpperCase() + name.substring(1);
        }).join(" ");

        lname = lname.map((name) => {
            return name[0].toUpperCase() + name.substring(1);
        }).join(" ");

        fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: fname,
                lastName: lname,
                email: email,
                mobileNumber: mobileNumber,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                setIsLoading(false);
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "You have successfully registered! Please login your account."
                },
                function(isConfirm) {
                    if(isConfirm) {
                        navigate("/");
                    }
                });
            }
            else {
                Swal.fire({
                    title: "Failed!",
                    icon: "error",
                    text: "Oops! Something went wrong! Try again."
                });
            }
        });

        
    }

    const emailExist = (e) => {
        e.preventDefault();
        setIsLoading(true);

        fetch(`${process.env.REACT_APP_API_URL}/user/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                setIsLoading(false);
                Swal.fire({
                    title: "Invalid emai!",
                    icon: "error",
                    text: "Duplicate email found! Please use a different email."
                });
            }
            else {
                register();
            }
        });
    };

    useEffect(() => {
        if (((firstName !== "") && (nameReg.test(firstName))) && ((lastName !== "") && (nameReg.test(lastName))) && (email !== "" && password1 !== "" && password2 !== "") && ((mobileNumber !== "") && (mobileReg.test(mobileNumber)) && (mobileNumber.length >= 11)) && (password1 === password2) && (password1.length && password2.length >= 8) && (passReg.test(password1) && passReg.test(password2) && emailReg.test(email))) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    },[firstName, lastName, email, mobileNumber, password1, password2, nameReg, passReg, emailReg, mobileReg]);

    return (
        (user.id !== null)
        ?
            <Navigate to="/" />
        :
        (isLoading)
        ?
        <Loading />
        :
        <Container id="signupForm" className="pt-5">
            <Row>
                <h4 className="title">Create your E-Commerce!</h4>
            </Row>
            <Form onSubmit={emailExist}>
            <Row>
                <Col xm={12} md={6}>
                    <Form.Group controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter your first name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
                    </Form.Group>

                    <Form.Group controlId="lastName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter your last name" value={lastName} onChange={e => setLastName(e.target.value)} required />
                    </Form.Group>

                    <Form.Group controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter your last name" value={email} onChange={e => setEmail(e.target.value)} required />
                    </Form.Group>

                    <Form.Group controlId="mobileNumber">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control type="text" placeholder="Enter your mobile number" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)} required />
                    </Form.Group>
                </Col>

                <Col xm={12} md={6}>
                    <Form.Group controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Enter your password" value={password1} onChange={e => setPassword1(e.target.value)} required />
                    </Form.Group>

                    <Form.Group controlId="password2">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control type="password" placeholder="Verify password" value={password2} onChange={e => setPassword2(e.target.value)} required />
                    </Form.Group>

                    <div className="d-flex flex-column gap-3 justify-content-around">
                    { isActive
                        ?
                            <Button variant="accent-2" type="submit" id="submitBtn" className="my-3">Submit</Button>
                        :
                            <Button variant="accent-2" type="submit" id="submitBtn" className="my-3" disabled>Submit</Button>
                    }
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>
    );
}