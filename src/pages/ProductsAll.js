import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import Loading from "../components/Loading";

export default function ProductsAll() {
    const [ products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/view/all`, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} product={product} />
                );
            }));
            setIsLoading(false);
        })
    }, []);
    return (
        (isLoading)
        ?
        <Loading />
        :
        <>
            {products}
        </>
    );
}