import "../assets/css/admin/AddProduct.css";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Form, InputGroup, Row } from "react-bootstrap";
import Swal from "sweetalert2";

export default function AddProduct() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [brand, setBrand] = useState("");
    const [category, setCategory] = useState("");
    const [price, setPrice] = useState("");
    const [stock, setStock] = useState("");
    const [isActive, setIsActive] = useState(false);
    const numReg = RegExp(/^[0-9\b]+$/);
    const [imageURL, setImageURL] = useState("");

    function addNewProduct(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/admin/addProduct`, {
            method: 'POST',
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: name,
                description: description,
                brand: brand,
                category: category,
                price: price,
                stock: stock,
                thumbnail: imageURL
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product has been created!"
                });

                setName("");
                setDescription("");
                setBrand("");
                setCategory("");
                setPrice("");
                setStock("");
                setImageURL("");
                navigate('/');
            }
            else {
                Swal.fire({
                    title: "Failed!",
                    icon: "error",
                    text: "Oops! Something went wrong! Try again."
                });
            }
        })
    }

    const uploadImage = (e) => {
        let data = new FormData();
        data.append("file", e.target.files[0]);
        data.append("upload_preset", "cfgktdeq");
        data.append("cloud_name", "dbpgyvd0f");

        fetch("https://api.cloudinary.com/v1_1/dbpgyvd0f/image/upload", {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(data => {
            setImageURL(data.url);
        })
    }
    
    useEffect(() => {
        if ((name !== "" && description !== "" && brand !== "" && category !== "" && price !== "" && stock !== "" && imageURL !== "") && numReg.test(price) && numReg.test(stock)) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    }, [name, description, brand, category, price, stock, numReg, imageURL]);

    return (
        (user.id !== null && user.isAdmin === false)
        ?
        <Navigate to="/" />
        :
        <Container id="addProductForm" className="mt-5">
            <Row>
                <h4 className="title">Add Product</h4>
            </Row>
            <Form onSubmit={addNewProduct}>
                <Row>
                    <Col xs={12} md={6}>
                        <Form.Group className="form-group" controlId="name">
                            <Form.Label>Product name</Form.Label>
                            <Form.Control type="text" placeholder="Enter the name of the product" value={name} onChange={e => setName(e.target.value)} required />
                        </Form.Group>
                        <Form.Group className="form-group" controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control as="textarea" aria-label="description" value={description} onChange={e => setDescription(e.target.value)} required />
                        </Form.Group>
                        <Form.Group className="form-group" controlId="brand">
                            <Form.Label>Brand</Form.Label>
                            <Form.Control type="text" placeholder="Enter the name of the product" value={brand} onChange={e => setBrand(e.target.value)} required />
                        </Form.Group>
                        <Form.Group className="form-group" controlId="category">
                            <Form.Label>Category</Form.Label>
                            <Form.Control type="text" placeholder="Enter the name of the product" value={category} onChange={e => setCategory(e.target.value)} required />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={6}>
                        <Form.Group className="form-group row">
                            <Col xs={6}>
                                <Form.Group className="form-group" controlId="price">
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={price} onChange={e => setPrice(e.target.value)} required />
                                </Form.Group>
                            </Col>

                            <Col xs={6}>
                                <Form.Group className="form-group" controlId="stock">
                                    <Form.Label>Stock</Form.Label>
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={stock} onChange={e => setStock(e.target.value)} required />
                                </Form.Group>
                            </Col>
                        </Form.Group>

                        <Form.Group className="form-group" controlId="thumbnailFile">
                            <Form.Label>Thumbnail</Form.Label>
                            <InputGroup>
                                <Form.Control type="file" onChange={e => uploadImage(e)} required />
                            </InputGroup>
                        </Form.Group>

                        <div className="d-flex flex-row gap-2 justify-content-md-end justify-content-center">
                            <Button type="reset" variant="danger" id="btnClear" className="w-25">Clear</Button>
                            { isActive
                            ?
                            <Button type="submit" variant="accent-2" id="btnAddProduct" className="w-25">ADD</Button>
                            :
                            <Button type="submit" variant="accent-2" id="btnAddProduct" className="w-25" disabled>ADD</Button>
                            }
                        </div>
                    </Col>
                </Row>
            </Form>
        </Container>
    );
}