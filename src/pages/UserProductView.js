import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import "../assets/css/ProductView.css";
import { useNavigate, useParams } from "react-router-dom";
import { Button, Col, Container, Form, Image, InputGroup, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function UserProductView() {
    const { user } = useContext(UserContext);
    const { productId } = useParams();
    const navigate = useNavigate();
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [brand, setBrand] = useState("");
    const [category, setCategory] = useState("");
    const [price, setPrice] = useState(0);
    const [discount, setDiscount] = useState(0);
    const [stock, setStock] = useState(0);
    const [quantity, setQuantity] = useState(0);
    const [thumbnail, setThumbnail] = useState("");
    const [discountedPrice, setDiscountedPrice] = useState(0);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/view/${productId}`)
        .then(res => res.json())
        .then(data => {
            setTitle(data.title);
            setDescription(data.description);
            setBrand(data.brand);
            setCategory(data.category);
            setPrice(data.price);
            setDiscount(data.discount);
            setStock(data.stock);
            setThumbnail(data.thumbnail);
        })
    }, [productId]);

    useEffect(() => {
        if (discount !== 0) {
            setDiscountedPrice(price*((100 - discount)/100));
        }
    }, [discount, price]);

    useEffect(() => {
        if (quantity === 0) {
            document.querySelector('.btnMinus').setAttribute('disabled', true);
        }
        else {
            document.querySelector('.btnMinus').removeAttribute('disabled');
        }

        if (quantity === stock) {
            document.querySelector('.btnAdd').setAttribute('disabled', true);
        }
        else {
            document.querySelector('.btnAdd').removeAttribute('disabled');
        }
    }, [quantity, stock]);

    const toPrice = (number) => {
        let num = number.toFixed(2);
        num = num.split('.');
        let dec = num[1];
        num = parseInt(num);
        num = num.toLocaleString(undefined, {maximumFractionDigits: 0});
        return `${num}.${dec}`;
    };

    const addToCart = (e) => {
        e.preventDefault();
        if (user.id === null) {
            localStorage.setItem('prevPage',`/product/${productId}`);
            Swal.fire({
                title: 'Error!',
                icon: "error",
                text: "You must login first to access this feature."
            });
            navigate("/login");
        }
        else {
            if (quantity !== 0) {
                fetch(`${process.env.REACT_APP_API_URL}/user/addToCart`, {
                    method: 'POST',
                    headers: {
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        productId: productId,
                        quantity: quantity,
                        subtotal: quantity * price
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data === true) {
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Item has been added to cart!"
                        });

                        setQuantity(0);
                    }
                    else {
                        Swal.fire({
                            title: "Failed!",
                            icon: "error",
                            text: "Something went wrong! Please try again."
                        });
                    }
                })
            }
            else {
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Please declare the quantity of item"
                });
            }
        }
    };

    const checkAddressExist = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/user/checkDeliveryAddress`, {
            method: 'POST',
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                checkout(e);
            }
            else {
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "You must have a delivery address before checking out a product!"
                }).then(result => {
                    if (result.isConfirmed) {
                        navigate("/user/addressBook");
                    }
                });
            }
        })
    }

    const checkout = (e) => {
        e.preventDefault();
        
        if (user.id === null) {
            localStorage.setItem('prevPage',`/product/${productId}`);
            Swal.fire({
                title: 'Error!',
                icon: "error",
                text: "You must login first to access this feature."
            });
            navigate("/login");
        }
        else {
            if (quantity !== 0) {
                let total = 0;
                if (discount !== 0) {
                    total = (price*((100 - discount)/100)).toFixed(4);
                }
                else {
                    total = price.toFixed(4);
                }
                fetch(`${process.env.REACT_APP_API_URL}/user/checkout`, {
                    method: 'POST',
                    headers: {
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        productId: productId,
                        quantity: quantity,
                        total: quantity * total,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data === true) {
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Item has been ordered!"
                        });

                        setQuantity(0);
                    }
                    else {
                        Swal.fire({
                            title: "Failed!",
                            icon: "error",
                            text: "Something went wrong! Please try again."
                        });
                    }
                })
            }
            else {
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Please declare the quantity of item"
                });
            }
        }
    };

    const subQty = (e) => {
        e.preventDefault();
        if (quantity !== 0) {
            setQuantity(quantity - 1);
        }
    }
    
    const addQty = (e) => {
        e.preventDefault();
        if (quantity !== stock) {
            setQuantity(quantity + 1);
        }
    }

    return (
        <Container className="mt-5">
            <Row>
                <Col xs={{ span: 8, offset: 2 }} md={{ span: 4, offset: 0 }} className="text-center text-md-start item-images">
                    <Image src={thumbnail} className="prodThumbnail" />
                </Col>
                <Col xs={12} md={8} className="item-details">
                    <h3 className="title">{title}</h3>
                    <p className="description">{description}</p>
                    <p className="brand"><b>Brand:</b> {brand !== "" ? brand : "No brand"}</p>
                    <p className="category"><b>Category:</b> {category !== "" ? category : "No category"}</p>
                    <p className="price">
                        <span className="currency">&#8369;</span>&ensp;{ discount !== 0 ? toPrice(discountedPrice) : toPrice(price) }
                    </p>
                    {discount !== 0
                    ?
                    <p className="discount">
                        <span className="text-decoration-line-through">
                            <span className="currency">&#8369;</span>&nbsp;{toPrice(price)}
                        </span>
                        &ensp;&ndash;{discount}&#37;
                    </p>
                    :
                    <></>
                    }
                    <Row className="justify-content-end align-items-center quantity mb-3">
                        <Col xs={9} className="justify-content-end text-end">
                            <p className="fw-bold m-0">Quantity:</p>
                        </Col>
                        <Col xs={3}>
                            <InputGroup>
                                <Button variant="accent-2" className="btnMinus" onClick={subQty}><FontAwesomeIcon icon="fa-solid fa-minus" /></Button>
                                <Form.Control aria-label="quantity" value={quantity} onChange={e => setQuantity(e.target.value)} className="text-center" />
                                <Button variant="accent-2" className="btnAdd" onClick={addQty}><FontAwesomeIcon icon="fa-solid fa-plus" /></Button>
                            </InputGroup>
                        </Col>
                    </Row>
                    <Row className="justify-content-center justify-content-lg-end gap-4">
                        <Button variant="outline-accent" className="btn-checkOut w-25 rounded-0" onClick={checkAddressExist}>Buy Now</Button>
                        <Button variant="accent-2" className="btn-addToCart w-25 rounded-0" onClick={addToCart}>Add to Cart</Button>
                    </Row>
                </Col>
            </Row>
        </Container>
    );
}