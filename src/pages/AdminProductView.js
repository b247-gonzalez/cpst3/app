import Loading from "../components/Loading";
import { useEffect, useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function AdminProductView() {
    const { productId } = useParams();
    const navigate = useNavigate();
    const [isLoading, setIsLoading] = useState(true);
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [brand, setBrand] = useState("");
    const [category, setCategory] = useState("");
    const [price, setPrice] = useState("");
    const [discount, setDiscount] = useState("");
    const [stock, setStock] = useState("");
    const [thumbnail, setThumbnail] = useState("");
    const [status, setStatus] = useState("");
    const [editMode, setEditMode] = useState(false);
    const [isSaveable, setIsSaveable] = useState(false);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/view/${productId}`)
        .then(res => res.json())
        .then(data => {
            setIsLoading(false);
            setTitle(data.title);
            setDescription(data.description);
            setBrand(data.brand);
            setCategory(data.category);
            setPrice(data.price);
            setDiscount(data.discount);
            setStock(data.stock);
            setThumbnail(data.thumbnail);

            if(data.isActive === true) {
                setStatus("Active");
            }
            else {
                setStatus("Archived")
            }
        })
    }, [productId]);

    useEffect(() => {
        if((title !== "") && (description !== "") && (brand !== "") && (category !== "") && (price !== "") && (stock !== "") && (thumbnail !== "")) {
            setIsSaveable(true);
        }
        else {
            setIsSaveable(false);
        }
    }, [title, description, brand, category, price, stock, thumbnail]);

    const changeMode = (e) => {
        e.preventDefault();
        if (editMode === false) {
            setEditMode(true);
        }
        else {
            setEditMode(false);
            fetch(`${process.env.REACT_APP_API_URL}/product/view/${productId}`)
            .then(res => res.json())
            .then(data => {
                setTitle(data.title);
                setDescription(data.description);
                setBrand(data.brand);
                setCategory(data.category);
                setPrice(data.price);
                setDiscount(data.discount);
                setStock(data.stock);
                setThumbnail(data.thumbnail);
            });
        }
    };

    const prodArchive = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/admin/product/archive/${productId}`, {
            method: 'PATCH',
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product has been archived!"
                });
                setStatus("Archived");
            }
            else {
                Swal.fire({
                    titel: "Failed!",
                    icon: "error",
                    text: "Somthing went wrong! Try again."
                });
            }
        });
    };

    const prodActivate = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/admin/product/activate/${productId}`, {
            method: 'PATCH',
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product has been activated!"
                });
                setStatus("Active");
            }
            else {
                Swal.fire({
                    titel: "Failed!",
                    icon: "error",
                    text: "Somthing went wrong! Try again."
                });
            }
        });
    }

    const prodDelete = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/admin/product/delete/${productId}`, {
            method: 'DELETE',
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product has been deleted!"
                });
                navigate("/");
            }
            else {
                Swal.fire({
                    titel: "Failed!",
                    icon: "error",
                    text: "Somthing went wrong! Try again."
                });
            }
        });
    };
    
    const uploadImage = (e) => {
        setThumbnail("");
        let data = new FormData();
        data.append("file", e.target.files[0]);
        data.append("upload_preset", "nzyo8agt");
        data.append("cloud_name", "dbpgyvd0f");

        fetch("https://api.cloudinary.com/v1_1/dbpgyvd0f/image/upload", {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(data => {
            setThumbnail(data.url);
        })
    }

    const prodSave = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/admin/product/edit/${productId}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                title: title,
                description: description,
                brand: brand,
                category: category,
                price: price,
                discount: discount,
                stock: stock,
                thumbnail: thumbnail
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product details has been updated!"
                });
                navigate('/');
            }
            else {
                Swal.fire({
                    title: "Failed!",
                    icon: "error",
                    text: "Something went wrong! Try again."
                });
            }
        });
    };

    return (
        (isLoading)
        ?
        <Loading />
        :
        <Container className="mt-5">
        <Row>
        <h4>Status:&nbsp;
            {status === "Active" 
            ?
                <font color="#02b562">{status}</font>
            :
                <font color="#dc3545">{status}</font>
            }
        </h4>
        </Row>
            <Form onSubmit={editMode ? prodSave : prodDelete} className="form">
                <Row>
                    <Col xs={12} md={6}>
                        <Form.Group className="form-group" controlId="title">
                            <Form.Label>Product Name</Form.Label>
                            {editMode
                            ?
                            <Form.Control type="text" placeholder="Enter the name of the product" value={title} onChange={e => setTitle(e.target.value)} required />
                            :
                            <Form.Control type="text" placeholder="Enter the name of the product" value={title} disabled />
                            }
                        </Form.Group>
                        <Form.Group className="form-group" controlId="description">
                            <Form.Label>Description</Form.Label>
                            {editMode
                            ?
                            <Form.Control as="textarea" aria-label="description" value={description} onChange={e => setDescription(e.target.value)} required />
                            :
                            <Form.Control as="textarea" aria-label="description" value={description} disabled />
                            }
                        </Form.Group>
                        <Form.Group className="form-group" controlId="brand">
                            <Form.Label>Brand</Form.Label>
                            {editMode
                            ?
                            <Form.Control type="text" placeholder="Enter the name of the product" value={brand} onChange={e => setBrand(e.target.value)} required />
                            :
                            <Form.Control type="text" placeholder="Enter the name of the product" value={brand} disabled />
                            }
                        </Form.Group>
                        <Form.Group className="form-group" controlId="category">
                            <Form.Label>Category</Form.Label>
                            {editMode
                            ?
                            <Form.Control type="text" placeholder="Enter the name of the product" value={category} onChange={e => setCategory(e.target.value)} required />
                            :
                            <Form.Control type="text" placeholder="Enter the name of the product" value={category} disabled />
                            }
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={6} className="mt-3 mt-md-0">
                    <Form.Group className="form-group row">
                            <Col xs={6}>
                                <Form.Group className="form-group" controlId="price">
                                    <Form.Label>Price</Form.Label>
                                    {editMode
                                    ?
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={price} onChange={e => setPrice(e.target.value)} required />
                                    :
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={price} disabled />
                                    }
                                </Form.Group>
                            </Col>
                            <Col xs={3}>
                                <Form.Group className="form-group" controlId="discount">
                                    <Form.Label>Discount</Form.Label>
                                    {editMode
                                    ?
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={discount} onChange={e => setDiscount(e.target.value)} required />
                                    :
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={discount} disabled />
                                    }
                                </Form.Group>
                            </Col>
                            <Col xs={3}>
                                <Form.Group className="form-group" controlId="stock">
                                    <Form.Label>Stock</Form.Label>
                                    {editMode
                                    ?
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={stock} onChange={e => setStock(e.target.value)} required />
                                    :
                                    <Form.Control type="text" placeholder="Enter the name of the product" value={stock} disabled />
                                    }
                                </Form.Group>
                            </Col>
                        </Form.Group>
                        <Form.Group className="form-group" controlId="thumbnail">
                            <Form.Label>Thumbnail</Form.Label>
                            {editMode
                            ?
                            <Form.Control type="file" onChange={e => uploadImage(e)} />
                            :
                            <Form.Control type="text" placeholder="Enter product image url" value={thumbnail} disabled />
                            }
                        </Form.Group>
                        { editMode
                        ?
                        <div className="d-flex flex-row gap-2 justify-content-md-end justify-content-center">
                            <Button type="button" variant="danger" id="btnCancel" className="w-25" onClick={changeMode}>Cancel</Button>
                            {isSaveable
                            ?
                            <Button type="submit" variant="primary" id="btnSave" className="w-25">Save</Button>
                            :
                            <Button type="submit" variant="primary" id="btnSave" className="w-25" disabled>Save</Button>
                            }
                        </div>
                        :
                        <div className="d-flex flex-row gap-2 justify-content-md-end justify-content-center">
                        { status === "Active"
                        ?
                            <Button type="button" variant="outline-danger" id="btnArchive" className="w-25" onClick={prodArchive}>Archive</Button>
                        :
                            <Button type="button" variant="outline-success" id="btnActivate" className="w-25" onClick={prodActivate}>Activate</Button>
                        }
                            <Button type="submit" variant="danger" id="btnDelete" className="w-25">Delete</Button>
                            <Button type="button" variant="primary" id="btnEdit" className="w-25" onClick={changeMode}>Edit</Button>
                        </div>
                        }
                    </Col>
                </Row>
            </Form>
        </Container>
    );
}